extern crate tuple_storage;
extern crate docopt;

use tuple_storage::{Storage, Tuple};
use std::fs::OpenOptions;

const USAGE: &'static str = "
Manipulate Tuples stored in a Storage in a typesafe way.
Usage:
    tuple_storage create <file> <schema>
    tuple_storage show <file>
    tuple_storage info <file>
    tuple_storage insert <file> <tuple>
    tuple_storage append <file> <tuple>
    tuple_storage update <file> <tuple>
    tuple_storage search <file> [<filter>]
    tuple_storage remove <file> <filter>
    tuple_storage remove_all <file> <filter>
    tuple_storage (-h | --help)
    tuple_storage --version

Options:
    -h --help     Show this screen.
    --version     Show version.

Commands:
    show <file>                  Show Schema
    info <file>                  Debug the Storage
    insert <file> <tuple>        Sorted insert
    append <file> <tuple>        Fast append
    update <file> <tuple>        Unsorted update
    search <file> [<filter>]     Linear search
    remove <file> <filter>       Sorted remove
    remove_all <file> <filter>   Fast remove

Notes:
    The insert and remove commands keep the storage sorted.
";

fn main() {
    let version = "0.1.0".to_owned();
    let args = docopt::Docopt::new(USAGE)
                      .and_then(|dopt| dopt.version(Some(version)).parse())
                      .unwrap_or_else(|e| e.exit());
    //println!("{:?}", args);
    
    let path = args.get_str("<file>");
    let schema = args.get_str("<schema>");
    
    let file = OpenOptions::new()
                    .read(true).write(true).create(true)
                    .open(&path).unwrap();


    if args.get_bool("create") {
        let _ = Storage::create(file, schema).unwrap();
    } else 

    if args.get_bool("show") {
        let s = Storage::open(file).unwrap();
        println!("{}", s.schema_to_string());
    } else

    if args.get_bool("info") {
        let s = Storage::open(file).unwrap();
        println!("{:#?}", s);
    } else

    if args.get_bool("insert") {
        let mut s = Storage::open(file).unwrap();
        let t = &mut Tuple::from_str(&s, args.get_str("<tuple>"));
        s.insert(t).unwrap();
    } else

    if args.get_bool("append") {
        let mut s = Storage::open(file).unwrap();
        let t = &mut Tuple::from_str(&s, args.get_str("<tuple>"));
        s.append(t).unwrap();
    } else
    
    if args.get_bool("update") {
        let mut s = Storage::open(file).unwrap();
        let t = &mut Tuple::from_str(&s, args.get_str("<tuple>"));
        s.update(t).unwrap();
    } else
    
    if args.get_bool("search") {
        let s = Storage::open(file).unwrap();
        let filter = s.create_filter(args.get_str("<filter>")).unwrap();
        
        print!("[");
        let mut count = 0;
        for t in s.iter() {
            if filter.matches(&t) {
                print!("\n    {}", t.to_string());
                count += 1;
            }
        }
        if count > 0 {
            print!("\n");
        }
        println!("]\nfound {} Tuples", count);
    
    } else

    if args.get_bool("remove") {
        let mut s = Storage::open(file).unwrap();
        let filter = s.create_filter(args.get_str("<filter>")).unwrap();
        let removed = s.remove(&filter);
        println!("removed {} Tuples", removed);
    } else

    if args.get_bool("remove_all") {
        let mut s = Storage::open(file).unwrap();
        let filter = s.create_filter(args.get_str("<filter>")).unwrap();
        let removed = s.remove_all(&filter);
        println!("removed {} Tuples", removed);
    }
}

