use super::*;

pub struct StorageIterator<'a> {
    storage: &'a Storage,
    offset: u64,
}

impl<'a> StorageIterator<'a> {
    pub fn new(storage: &'a Storage) -> StorageIterator<'a> {
        StorageIterator {
            storage,
            offset: 0,
        }
    }
}

impl<'a> Iterator for StorageIterator<'a> {
    // we will be counting with usize
    type Item = Tuple;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(t) = self.storage.get(self.offset) {
            self.offset += 1;
            Some(t)
        } else {
            None
        }
    }
}
