//use super::*;


/// Wrapper Enum for supported types
#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub enum Ints {
    U64(u64), I64(i64),
    U32(u32), I32(i32),
    U16(u16), I16(i16),
     U8(u8) ,  I8(i8),
}
pub use Ints::*;

impl Ints {
    pub fn str_repr(&self) -> &str {
        match self {
            U64(_) => "u64",
            I64(_) => "i64",
            U32(_) => "u32",
            I32(_) => "i32",
            U16(_) => "u16",
            I16(_) => "i16",
            U8(_)  => "u8",
            I8(_)  => "i8",
        }
    }
}
