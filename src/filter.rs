use super::*;

#[derive(Debug)]
pub enum Filter {
    /// No filter
    Any,
    Less(Ints),
    Equal(Ints),
    Greater(Ints),
}
use Filter::*;

impl Filter {
    pub fn create(schema: &Schema, filter: &str) -> Result<Filter, SchemaParseError> {
        let filter = filter.trim();
        if filter == "" {
            return Ok(Any)
        }
        
        let (operand, rest) = filter.split_at(1);
        let filter_schema = (&format!("({})", schema.schema[0])[..]).parse()?;
        let tuple = Tuple::from_schema_and_str(&filter_schema, rest);
        let num = tuple.data[0].clone();
        
        Ok(match operand {
            "<" => Less(num),
            "=" => Equal(num),
            ">" => Greater(num),
            _ => return Err(SchemaParseError::FilterInvalidOperand(operand.into()))
        })
    }
    
    pub fn matches(&self, tuple: &Tuple) -> bool {
        match self {
            Any => true,
            Less(i) => tuple.data[0] < *i,
            Equal(i) => tuple.data[0] == *i,
            Greater(i) => tuple.data[0] > *i,
        }
    }
}
