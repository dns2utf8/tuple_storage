# Changelog

## Unreleased

## 0.1.1

* Cosmetic changes and correct meta data

## 0.1.0

* Implement CRUD (Create, Show, Append, Insert, Update, Remove and RemoveAll)
