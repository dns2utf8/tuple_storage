# tuple_storage

[![pipeline status](https://gitlab.com/dns2utf8/tuple_storage/badges/master/build.svg)](https://gitlab.com/dns2utf8/tuple_storage/)

Store one kind of tuple per file with ease.
Every access is type-safe.

The database is memory mapped for fast access even as the set grows.


## Building from source

Clone this repo and make sure to have [rustup](https://rustup.rs/) installed then run this:

```bash
cargo run --release -- [optional arguments]
```

## Example usage

```bash
$ cargo run create target/demo.ts '(u8, i16, u32, i64)'
$ cargo run show target/demo.ts
(u8, i16, u32, i64)

$ cargo run insert target/demo.ts '(1, 2, 3, 4)'
$ cargo run insert target/demo.ts '(6, 7, 8, 9)'
$ cargo run insert target/demo.ts '(3, 4, 5, 6)'

$ cargo run search target/demo.ts
[
    (1, 2, 3, 4)
    (3, 4, 5, 6)
    (6, 7, 8, 9)
]
found 3 Tuples

$ cargo run search target/demo.ts '<3'
[
    (1, 2, 3, 4)
]
found 1 Tuples
$ cargo run search target/demo.ts '=3'
[
    (3, 4, 5, 6)
]
found 1 Tuples
$ cargo run search target/demo.ts '>3'
[
    (6, 7, 8, 9)
]
found 1 Tuples

$ cargo run remove target/demo.ts '>3'
removed 1 Tuples
$ cargo run search target/demo.ts '>3'
[]
found 0 Tuples


# inserting Tuple violating the Schema:
$ cargo run insert target/demo.ts '(-23, 0, 0, 0)'
thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: ParseIntError { kind: InvalidDigit }', libcore/result.rs:945:5
note: Run with `RUST_BACKTRACE=1` for a backtrace.

# searching with an invalid search index
$ cargo run search target/demo.ts '=-2'
thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: ParseIntError { kind: InvalidDigit }', libcore/result.rs:945:5
note: Run with `RUST_BACKTRACE=1` for a backtrace.
```
